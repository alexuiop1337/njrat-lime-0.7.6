﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Builder
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.bsod = New System.Windows.Forms.CheckBox()
        Me.T1 = New System.Windows.Forms.TextBox()
        Me.Isf = New System.Windows.Forms.CheckBox()
        Me.Isu = New System.Windows.Forms.CheckBox()
        Me.klen = New System.Windows.Forms.NumericUpDown()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Anti_CH = New System.Windows.Forms.CheckBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.exe = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.dir = New System.Windows.Forms.ComboBox()
        Me.Idr = New System.Windows.Forms.CheckBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.CKUpx = New System.Windows.Forms.CheckBox()
        Me.USB_SP = New System.Windows.Forms.CheckBox()
        Me.CKOBF = New System.Windows.Forms.CheckBox()
        Me.BOT_KILL = New System.Windows.Forms.CheckBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.SLP = New System.Windows.Forms.NumericUpDown()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Y = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.VN = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.port = New System.Windows.Forms.NumericUpDown()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.host = New System.Windows.Forms.TextBox()
        Me.HIDE_ME = New System.Windows.Forms.CheckBox()
        Me.Persis = New System.Windows.Forms.CheckBox()
        Me.TOSCHK = New System.Windows.Forms.CheckBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.vischk = New System.Windows.Forms.CheckBox()
        Me.TMOT = New System.Windows.Forms.NumericUpDown()
        Me.Label10 = New System.Windows.Forms.Label()
        CType(Me.klen, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.SLP, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.port, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TMOT, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button1.Enabled = False
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Arial Black", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Lime
        Me.Button1.Location = New System.Drawing.Point(6, 425)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(380, 135)
        Me.Button1.TabIndex = 6
        Me.Button1.Text = ":: Build ::"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'bsod
        '
        Me.bsod.AutoSize = True
        Me.bsod.Cursor = System.Windows.Forms.Cursors.Hand
        Me.bsod.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bsod.ForeColor = System.Drawing.Color.Lime
        Me.bsod.Location = New System.Drawing.Point(411, 48)
        Me.bsod.Margin = New System.Windows.Forms.Padding(4)
        Me.bsod.Name = "bsod"
        Me.bsod.Size = New System.Drawing.Size(227, 23)
        Me.bsod.TabIndex = 11
        Me.bsod.Text = "Protect Process [BSOD]"
        Me.bsod.UseVisualStyleBackColor = True
        '
        'T1
        '
        Me.T1.BackColor = System.Drawing.Color.Black
        Me.T1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.T1.ForeColor = System.Drawing.Color.Lime
        Me.T1.Location = New System.Drawing.Point(139, 610)
        Me.T1.Margin = New System.Windows.Forms.Padding(4)
        Me.T1.Multiline = True
        Me.T1.Name = "T1"
        Me.T1.Size = New System.Drawing.Size(149, 30)
        Me.T1.TabIndex = 12
        Me.T1.Visible = False
        '
        'Isf
        '
        Me.Isf.AutoSize = True
        Me.Isf.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Isf.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Isf.ForeColor = System.Drawing.Color.Lime
        Me.Isf.Location = New System.Drawing.Point(411, 81)
        Me.Isf.Margin = New System.Windows.Forms.Padding(4)
        Me.Isf.Name = "Isf"
        Me.Isf.Size = New System.Drawing.Size(164, 23)
        Me.Isf.TabIndex = 13
        Me.Isf.Text = "Copy To Startup"
        Me.Isf.UseVisualStyleBackColor = True
        '
        'Isu
        '
        Me.Isu.AutoSize = True
        Me.Isu.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Isu.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Isu.ForeColor = System.Drawing.Color.Lime
        Me.Isu.Location = New System.Drawing.Point(411, 114)
        Me.Isu.Margin = New System.Windows.Forms.Padding(4)
        Me.Isu.Name = "Isu"
        Me.Isu.Size = New System.Drawing.Size(152, 23)
        Me.Isu.TabIndex = 14
        Me.Isu.Text = "Registy Starup"
        Me.Isu.UseVisualStyleBackColor = True
        '
        'klen
        '
        Me.klen.BackColor = System.Drawing.Color.Black
        Me.klen.ForeColor = System.Drawing.Color.Lime
        Me.klen.Location = New System.Drawing.Point(553, 477)
        Me.klen.Margin = New System.Windows.Forms.Padding(4)
        Me.klen.Maximum = New Decimal(New Integer() {512, 0, 0, 0})
        Me.klen.Minimum = New Decimal(New Integer() {5, 0, 0, 0})
        Me.klen.Name = "klen"
        Me.klen.Size = New System.Drawing.Size(79, 26)
        Me.klen.TabIndex = 15
        Me.klen.Value = New Decimal(New Integer() {20, 0, 0, 0})
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Lime
        Me.Label6.Location = New System.Drawing.Point(405, 480)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(126, 19)
        Me.Label6.TabIndex = 16
        Me.Label6.Text = "KeyLogs in KB"
        '
        'Anti_CH
        '
        Me.Anti_CH.AutoSize = True
        Me.Anti_CH.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Anti_CH.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Anti_CH.ForeColor = System.Drawing.Color.Lime
        Me.Anti_CH.Location = New System.Drawing.Point(411, 213)
        Me.Anti_CH.Margin = New System.Windows.Forms.Padding(4)
        Me.Anti_CH.Name = "Anti_CH"
        Me.Anti_CH.Size = New System.Drawing.Size(79, 23)
        Me.Anti_CH.TabIndex = 17
        Me.Anti_CH.Text = "Anti's"
        Me.Anti_CH.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Lime
        Me.Label3.Location = New System.Drawing.Point(4, 24)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(85, 19)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "File Name"
        '
        'exe
        '
        Me.exe.BackColor = System.Drawing.Color.Black
        Me.exe.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.exe.Enabled = False
        Me.exe.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.exe.ForeColor = System.Drawing.Color.Lime
        Me.exe.Location = New System.Drawing.Point(9, 49)
        Me.exe.Margin = New System.Windows.Forms.Padding(4)
        Me.exe.Name = "exe"
        Me.exe.Size = New System.Drawing.Size(360, 26)
        Me.exe.TabIndex = 5
        Me.exe.Text = "Client.exe"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Lime
        Me.Label4.Location = New System.Drawing.Point(4, 88)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(126, 19)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Drop Directory"
        '
        'dir
        '
        Me.dir.BackColor = System.Drawing.Color.Black
        Me.dir.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.dir.Enabled = False
        Me.dir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.dir.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dir.ForeColor = System.Drawing.Color.Lime
        Me.dir.FormattingEnabled = True
        Me.dir.Items.AddRange(New Object() {"%TEMP%", "%AppData%", "%UserProfile%", "%AllUsersProfile%", "%WinDir%"})
        Me.dir.Location = New System.Drawing.Point(9, 112)
        Me.dir.Margin = New System.Windows.Forms.Padding(4)
        Me.dir.Name = "dir"
        Me.dir.Size = New System.Drawing.Size(360, 27)
        Me.dir.TabIndex = 7
        '
        'Idr
        '
        Me.Idr.AutoSize = True
        Me.Idr.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Idr.ForeColor = System.Drawing.Color.Lime
        Me.Idr.Location = New System.Drawing.Point(294, 18)
        Me.Idr.Margin = New System.Windows.Forms.Padding(4)
        Me.Idr.Name = "Idr"
        Me.Idr.Size = New System.Drawing.Size(71, 24)
        Me.Idr.TabIndex = 14
        Me.Idr.Text = "Copy"
        Me.Idr.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Idr)
        Me.GroupBox1.Controls.Add(Me.dir)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.exe)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Location = New System.Drawing.Point(6, 253)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Size = New System.Drawing.Size(380, 161)
        Me.GroupBox1.TabIndex = 5
        Me.GroupBox1.TabStop = False
        '
        'CKUpx
        '
        Me.CKUpx.AutoSize = True
        Me.CKUpx.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CKUpx.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CKUpx.ForeColor = System.Drawing.Color.Lime
        Me.CKUpx.Location = New System.Drawing.Point(411, 346)
        Me.CKUpx.Margin = New System.Windows.Forms.Padding(4)
        Me.CKUpx.Name = "CKUpx"
        Me.CKUpx.Size = New System.Drawing.Size(202, 23)
        Me.CKUpx.TabIndex = 20
        Me.CKUpx.Text = "MPress Compression"
        Me.CKUpx.UseVisualStyleBackColor = True
        '
        'USB_SP
        '
        Me.USB_SP.AutoSize = True
        Me.USB_SP.Cursor = System.Windows.Forms.Cursors.Hand
        Me.USB_SP.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.USB_SP.ForeColor = System.Drawing.Color.Lime
        Me.USB_SP.Location = New System.Drawing.Point(411, 246)
        Me.USB_SP.Margin = New System.Windows.Forms.Padding(4)
        Me.USB_SP.Name = "USB_SP"
        Me.USB_SP.Size = New System.Drawing.Size(131, 23)
        Me.USB_SP.TabIndex = 18
        Me.USB_SP.Text = "Spread USB"
        Me.USB_SP.UseVisualStyleBackColor = True
        '
        'CKOBF
        '
        Me.CKOBF.AutoSize = True
        Me.CKOBF.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CKOBF.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CKOBF.ForeColor = System.Drawing.Color.Lime
        Me.CKOBF.Location = New System.Drawing.Point(411, 313)
        Me.CKOBF.Margin = New System.Windows.Forms.Padding(4)
        Me.CKOBF.Name = "CKOBF"
        Me.CKOBF.Size = New System.Drawing.Size(130, 23)
        Me.CKOBF.TabIndex = 19
        Me.CKOBF.Text = "Obfuscation"
        Me.CKOBF.UseVisualStyleBackColor = True
        '
        'BOT_KILL
        '
        Me.BOT_KILL.AutoSize = True
        Me.BOT_KILL.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BOT_KILL.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BOT_KILL.ForeColor = System.Drawing.Color.Lime
        Me.BOT_KILL.Location = New System.Drawing.Point(411, 280)
        Me.BOT_KILL.Margin = New System.Windows.Forms.Padding(4)
        Me.BOT_KILL.Name = "BOT_KILL"
        Me.BOT_KILL.Size = New System.Drawing.Size(103, 23)
        Me.BOT_KILL.TabIndex = 20
        Me.BOT_KILL.Text = "BotKiller"
        Me.BOT_KILL.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.Lime
        Me.Label7.Location = New System.Drawing.Point(405, 395)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(103, 19)
        Me.Label7.TabIndex = 22
        Me.Label7.Text = "Sleep in sec"
        '
        'SLP
        '
        Me.SLP.BackColor = System.Drawing.Color.Black
        Me.SLP.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SLP.ForeColor = System.Drawing.Color.Lime
        Me.SLP.Location = New System.Drawing.Point(553, 393)
        Me.SLP.Margin = New System.Windows.Forms.Padding(4)
        Me.SLP.Maximum = New Decimal(New Integer() {9000000, 0, 0, 0})
        Me.SLP.Name = "SLP"
        Me.SLP.Size = New System.Drawing.Size(79, 26)
        Me.SLP.TabIndex = 23
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Y)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.VN)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.port)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.host)
        Me.GroupBox2.Location = New System.Drawing.Point(6, 2)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(380, 227)
        Me.GroupBox2.TabIndex = 24
        Me.GroupBox2.TabStop = False
        '
        'Y
        '
        Me.Y.BackColor = System.Drawing.Color.Black
        Me.Y.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Y.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Y.ForeColor = System.Drawing.Color.Lime
        Me.Y.Location = New System.Drawing.Point(10, 114)
        Me.Y.Margin = New System.Windows.Forms.Padding(4)
        Me.Y.Name = "Y"
        Me.Y.Size = New System.Drawing.Size(360, 26)
        Me.Y.TabIndex = 24
        Me.Y.Text = "|NYAN|"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.Lime
        Me.Label8.Location = New System.Drawing.Point(9, 89)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(39, 19)
        Me.Label8.TabIndex = 23
        Me.Label8.Text = "Key"
        '
        'VN
        '
        Me.VN.BackColor = System.Drawing.Color.Black
        Me.VN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.VN.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.VN.ForeColor = System.Drawing.Color.Lime
        Me.VN.Location = New System.Drawing.Point(10, 184)
        Me.VN.Margin = New System.Windows.Forms.Padding(4)
        Me.VN.Name = "VN"
        Me.VN.Size = New System.Drawing.Size(360, 26)
        Me.VN.TabIndex = 22
        Me.VN.Text = "Office"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Lime
        Me.Label5.Location = New System.Drawing.Point(9, 160)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(103, 19)
        Me.Label5.TabIndex = 21
        Me.Label5.Text = "Client Name"
        '
        'port
        '
        Me.port.BackColor = System.Drawing.Color.Black
        Me.port.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.port.ForeColor = System.Drawing.Color.Lime
        Me.port.Location = New System.Drawing.Point(285, 46)
        Me.port.Margin = New System.Windows.Forms.Padding(4)
        Me.port.Maximum = New Decimal(New Integer() {60000, 0, 0, 0})
        Me.port.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.port.Name = "port"
        Me.port.Size = New System.Drawing.Size(85, 26)
        Me.port.TabIndex = 20
        Me.port.Value = New Decimal(New Integer() {6522, 0, 0, 0})
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Lime
        Me.Label2.Location = New System.Drawing.Point(287, 22)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(43, 19)
        Me.Label2.TabIndex = 19
        Me.Label2.Text = "Port"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Lime
        Me.Label1.Location = New System.Drawing.Point(5, 22)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 19)
        Me.Label1.TabIndex = 18
        Me.Label1.Text = "Host"
        '
        'host
        '
        Me.host.BackColor = System.Drawing.Color.Black
        Me.host.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.host.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.host.ForeColor = System.Drawing.Color.Lime
        Me.host.Location = New System.Drawing.Point(8, 46)
        Me.host.Margin = New System.Windows.Forms.Padding(4)
        Me.host.Name = "host"
        Me.host.Size = New System.Drawing.Size(263, 26)
        Me.host.TabIndex = 17
        Me.host.Text = "127.0.0.1"
        '
        'HIDE_ME
        '
        Me.HIDE_ME.AutoSize = True
        Me.HIDE_ME.Cursor = System.Windows.Forms.Cursors.Hand
        Me.HIDE_ME.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.HIDE_ME.ForeColor = System.Drawing.Color.Lime
        Me.HIDE_ME.Location = New System.Drawing.Point(411, 147)
        Me.HIDE_ME.Margin = New System.Windows.Forms.Padding(4)
        Me.HIDE_ME.Name = "HIDE_ME"
        Me.HIDE_ME.Size = New System.Drawing.Size(120, 23)
        Me.HIDE_ME.TabIndex = 25
        Me.HIDE_ME.Text = "Hide Client"
        Me.HIDE_ME.UseVisualStyleBackColor = True
        '
        'Persis
        '
        Me.Persis.AutoSize = True
        Me.Persis.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Persis.Enabled = False
        Me.Persis.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Persis.ForeColor = System.Drawing.Color.Lime
        Me.Persis.Location = New System.Drawing.Point(411, 180)
        Me.Persis.Margin = New System.Windows.Forms.Padding(4)
        Me.Persis.Name = "Persis"
        Me.Persis.Size = New System.Drawing.Size(127, 23)
        Me.Persis.TabIndex = 26
        Me.Persis.Text = "Persistence"
        Me.Persis.UseVisualStyleBackColor = True
        '
        'TOSCHK
        '
        Me.TOSCHK.AutoSize = True
        Me.TOSCHK.Cursor = System.Windows.Forms.Cursors.Hand
        Me.TOSCHK.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TOSCHK.ForeColor = System.Drawing.Color.Lime
        Me.TOSCHK.Location = New System.Drawing.Point(6, 568)
        Me.TOSCHK.Margin = New System.Windows.Forms.Padding(4)
        Me.TOSCHK.Name = "TOSCHK"
        Me.TOSCHK.Size = New System.Drawing.Size(22, 21)
        Me.TOSCHK.TabIndex = 27
        Me.TOSCHK.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Arial", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.Lime
        Me.Label9.Location = New System.Drawing.Point(28, 566)
        Me.Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(43, 19)
        Me.Label9.TabIndex = 25
        Me.Label9.Text = "TOS"
        '
        'vischk
        '
        Me.vischk.AutoSize = True
        Me.vischk.Checked = True
        Me.vischk.CheckState = System.Windows.Forms.CheckState.Checked
        Me.vischk.Cursor = System.Windows.Forms.Cursors.Hand
        Me.vischk.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.vischk.ForeColor = System.Drawing.Color.Lime
        Me.vischk.Location = New System.Drawing.Point(411, 17)
        Me.vischk.Margin = New System.Windows.Forms.Padding(4)
        Me.vischk.Name = "vischk"
        Me.vischk.Size = New System.Drawing.Size(136, 23)
        Me.vischk.TabIndex = 28
        Me.vischk.Text = "Visible Client"
        Me.vischk.UseVisualStyleBackColor = True
        '
        'TMOT
        '
        Me.TMOT.BackColor = System.Drawing.Color.Black
        Me.TMOT.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TMOT.ForeColor = System.Drawing.Color.Lime
        Me.TMOT.Location = New System.Drawing.Point(553, 435)
        Me.TMOT.Margin = New System.Windows.Forms.Padding(4)
        Me.TMOT.Maximum = New Decimal(New Integer() {9000000, 0, 0, 0})
        Me.TMOT.Name = "TMOT"
        Me.TMOT.Size = New System.Drawing.Size(80, 26)
        Me.TMOT.TabIndex = 30
        Me.TMOT.Value = New Decimal(New Integer() {30, 0, 0, 0})
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.Lime
        Me.Label10.Location = New System.Drawing.Point(405, 437)
        Me.Label10.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(123, 19)
        Me.Label10.TabIndex = 29
        Me.Label10.Text = "Timeout in sec"
        '
        'Builder
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(645, 592)
        Me.Controls.Add(Me.TMOT)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.vischk)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.TOSCHK)
        Me.Controls.Add(Me.Persis)
        Me.Controls.Add(Me.HIDE_ME)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.CKUpx)
        Me.Controls.Add(Me.SLP)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.BOT_KILL)
        Me.Controls.Add(Me.CKOBF)
        Me.Controls.Add(Me.USB_SP)
        Me.Controls.Add(Me.Anti_CH)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.klen)
        Me.Controls.Add(Me.Isu)
        Me.Controls.Add(Me.Isf)
        Me.Controls.Add(Me.T1)
        Me.Controls.Add(Me.bsod)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Builder"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Builder"
        CType(Me.klen, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.SLP, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.port, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TMOT, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents bsod As System.Windows.Forms.CheckBox
    Friend WithEvents T1 As System.Windows.Forms.TextBox
    Friend WithEvents Isf As System.Windows.Forms.CheckBox
    Friend WithEvents Isu As System.Windows.Forms.CheckBox
    Friend WithEvents klen As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Anti_CH As CheckBox
    Friend WithEvents Label3 As Label
    Friend WithEvents exe As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents dir As ComboBox
    Friend WithEvents Idr As CheckBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents USB_SP As CheckBox
    Friend WithEvents CKOBF As CheckBox
    Friend WithEvents CKUpx As CheckBox
    Friend WithEvents BOT_KILL As CheckBox
    Friend WithEvents Label7 As Label
    Friend WithEvents SLP As NumericUpDown
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents VN As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents port As NumericUpDown
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents host As TextBox
    Friend WithEvents HIDE_ME As CheckBox
    Friend WithEvents Y As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents Persis As CheckBox
    Friend WithEvents TOSCHK As CheckBox
    Friend WithEvents Label9 As Label
    Friend WithEvents vischk As CheckBox
    Friend WithEvents TMOT As NumericUpDown
    Friend WithEvents Label10 As Label
End Class
